require 'rubygems'
require 'simple-rss'
require 'open-uri'

result = ""

urls = [
"http://blogs.yahoo.co.jp/s_jidokan/rss.xml",
"http://blogs.yahoo.co.jp/enana_dawaka/rss.xml",
"http://greenigusa.wixsite.com/mysite/feed.xml",
"http://blog.livedoor.jp/hantai_voice/index.rdf",
]

articles = []
urls.each do |url|
  rss = SimpleRSS.parse open(url)
  rss.items.each do |item|
    if item.pubDate
      articles << [rss.channel.title, item, item.pubDate]
    elsif item.dc_date
        articles << [rss.channel.title, item, item.dc_date]
    end
  end
end

articles = articles.sort { |a, b| b[2].to_i <=> a[2].to_i }
i = 0
articles.each do |article|
  i = i + 1
  if i <= 10
    if i > 1
      result += "                "
    end
    result += "<li class=\"list-group-item list-group-item-primary\"><a href=\"" + article[1].link.force_encoding("UTF-8") + "\" rel=\"external\" target=\"_blank\">" + article[1].title.force_encoding("UTF-8") + "</a>（" + article[0].force_encoding("UTF-8").gsub(/　/, "") + "、" + article[2].year.to_s + "年" + article[2].month.to_s + "月" + article[2].day.to_s + "日）</li>\n"
  end
end

begin
  File.open("./public/index.html", "r:utf-8") do |file|
    file.each_line do |line|
      puts line.gsub(/<!-- TOPICS -->/, result.force_encoding("UTF-8"))
    end
  end
end